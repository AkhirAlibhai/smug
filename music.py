''' Music cog, and other audio related classes '''

import asyncio
from contextlib import suppress
from ctypes.util import find_library
from functools import partial

import discord
from discord.ext import commands

import util

if not discord.opus.is_loaded():
    discord.opus.load_opus(find_library('opus'))

class AudioItem:
    ''' Single audio item that can be added to the AudioState queue '''
    def __init__(self, message, player):
        self.requester = message.author
        self.channel = message.channel
        self.player = player
        self.duration = divmod(self.player.duration, 60)

    def __str__(self):
        return (f'    Title: **{self.player.title}**\n'
                f'    Url: `{self.player.download_url}`\n'
                f'    Duration: {self.duration[0]:02}:{self.duration[1]:02}')

class AudioState:
    ''' Manager for an asyncio Queue of AudioItem instances '''
    def __init__(self, bot):
        self.current: AudioItem = None
        self.client: discord.VoiceClient = None
        self.bot = bot
        self.playNext = asyncio.Event()
        self.queue = asyncio.Queue()
        self.audioPlayer = self.bot.loop.create_task(self.audioPlayerTask())

    def advance(self):
        ''' Advances the queue - Provided as callback to each ytdl_player '''
        self.bot.loop.call_soon_threadsafe(self.playNext.set)

    def isPlaying(self):
        ''' Checks if there is currently any audio item being played '''
        if self.client is None or self.current is None:
            return False
        return not self.current.player.is_done()

    def skip(self):
        ''' Makes player task advance by stopping the current audio item '''
        # TODO: `playing` command still shows a skipped song (or finished song)
        #       as "now playing" if it was the last item in the queue. Need to
        #       make it so that a skipping/finishing a song sets current to None
        if self.isPlaying():
            self.current.player.stop()

    async def audioPlayerTask(self):
        ''' asyncio task that plays audio items from the queue '''
        while True:
            self.playNext.clear()
            self.current = await self.queue.get()
            await self.bot.send_message(
                self.current.channel,
                f'Now playing {self.current.player.title}')
            self.current.player.start()
            await self.playNext.wait()

class Music:
    ''' Music cog '''
    def __init__(self, bot):
        self.bot = bot
        self.audioState = AudioState(self.bot)

    async def createClient(self, channel):
        ''' Makes the bot join a specified voice channel '''
        self.audioState.client = await self.bot.join_voice_channel(channel)

    def __unload(self):
        ''' Auto-magically gets called when the cog is unloaded '''
        self.audioState.audioPlayer.cancel()
        if self.audioState.client:
            self.bot.loop.create_task(self.audioState.client.disconnect())

    @commands.command(pass_context=True)
    async def join(self, context, *, channel: discord.Channel = None):
        '''
        Makes the bot join a voice channel. Defaults to your current channel
        '''
        if channel:
            try:
                await self.createClient(channel)
            except discord.InvalidArgument:
                await self.bot.say('That is not a voice channel')
            except discord.ClientException:
                await self.bot.say('Already in a voice channel')
            else:
                await self.bot.say(f'Ready to play audio in {channel.name}')
            return # This execution path will not be invoked by the `play`
                   # command, so no return value is needed
        # Bot has been commanded to join the user's current channel
        channel = context.message.author.voice.voice_channel
        if channel:
            if self.audioState.client is None:
                joinVC = self.bot.join_voice_channel
                self.audioState.client = await joinVC(channel)
            else:
                await self.audioState.client.move_to(channel)
            return True # Ready to play
        await self.bot.say('You are not in an audio channel')
        return False # Not ready to play

    @commands.command(pass_context=True)
    async def leave(self, context):
        ''' Makes the bot leave the voice channel, and clears the queue '''
        if self.audioState.isPlaying():
            self.audioState.current.player.stop()
        with suppress(Exception):
            self.audioState.client.cancel()
            await self.audioState.client.disconnect()

    @commands.command(pass_context=True)
    async def play(self, context, *, request: str = ''):
        ''' Plays/queues a song '''
        # TODO: If a URL is not provided, return options from a search, and
        #       allow the user to choose one
        # TODO: playlist support - should be a subcommand of play:
        #       i.e. `!play playlist <playlist name, url, or preset>`
        # TODO: make this command resume a paused song if there is no request
        with suppress(Exception):
            # Attempts to load the request url from the configured presets
            request = util.AUDIO_PRESETS[request]
            if type(request) == list:
                # TODO: Implement add playlist to queue feature
                await self.bot.say('Playlist support not yet implemented')
                return
        options = dict(
            default_search='auto',
            quiet=True
            )
        if self.audioState.client is None:
            success = await context.invoke(self.join)
            if not success:
                return
        try:
            player = await self.audioState.client.create_ytdl_player(
                request, ytdl_options=options, after=self.audioState.advance)
        except Exception as error:
            errorMsg = util.markdownCodeBlock(
                f'{type(error).__name__}: {error}',
                language='Python')
            await self.bot.longSay(
                f'An error occurred while processing this request:\n{errorMsg}')
        else:
            player.volume = 0.5 # TODO: get this value from config.yaml
            entry = AudioItem(context.message, player)
            await self.bot.say(f'Added **{entry.player.title}** to the queue')
            await self.audioState.queue.put(entry)

    @commands.command(pass_context=True)
    async def skip(self, context):
        ''' Skips the current song '''
        if not self.audioState.isPlaying():
            await self.bot.say('Nothing is playing right now')
            return
        else:
            await self.bot.say('Skipping song')
            self.audioState.skip()

    @commands.command(pass_context=True)
    async def pause(self, context):
        ''' Pauses the current song '''
        if not self.audioState.isPlaying():
            await self.bot.say('Nothing is playing right now')
            return
        await self.bot.say('Pausing song')
        self.audioState.current.player.pause()

    @commands.command(pass_context=True)
    async def resume(self, context):
        ''' Resumes the paused song '''
        if not self.audioState.isPlaying():
            await self.bot.say('Nothing is playing right now')
            return
        await self.bot.say('Resuming song')
        self.audioState.current.player.resume()

    @commands.command()
    async def playing(self):
        ''' Prints info about the currently playing song, and the queue '''
        if self.audioState.current is None:
            await self.bot.say('Nothing is playing')
            return
        queueInfo = [f'Now playing:', str(self.audioState.current)]
        # TODO: Print the queue when this command is called
        # if not self.audioState.queue.empty():
        #     queueInfo.append('\n\nSongs in queue:\n')
        #     queueInfo.extend(map(str, self.audioState.queue))
        await self.bot.longSay('\n'.join(queueInfo))

    # TODO: this command
    # @commands.command(pass_context=True)
    # async def devilman(self, context):
    #     playDevilman = partial(self.play,
    #         request='https://www.youtube.com/watch?v=diuexInkshA')
    #     await context.invoke(playDevilman)

def setup(bot):
    ''' Adds the music cog to the bot '''
    bot.add_cog(Music(bot))

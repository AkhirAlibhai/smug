#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

''' Main bot script. Executing this file runs the bot '''

import logging

from discord.ext import commands

import util

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)
LOGGER.addHandler(logging.FileHandler(
    filename=f'{__name__}.log',
    encoding='utf-8',
    mode='w'
))

STARTUP_EXTENSIONS = ['commands', 'music']
DESCRIPTION = 'Multipurpose Discord chat/voice bot'
BOT = commands.Bot(command_prefix='!', description=DESCRIPTION)

@BOT.event
async def on_ready():
    ''' Invoked when the bot is ready for user interactions '''
    print(f'Logged in as:\n{BOT.user.name}\n{BOT.user.id}\n------------------')

async def longSay(self, fullMessage, useCodeBlocks=False):
    '''
    Allows the bot to send messages longer than the 2000 character limit imposed
    by Discord by sending the message as a series of (up to) 2000 character
    messages.
    Sends up to messageLimit messages.
    '''
    messageLimit = 10 # 2000 characters is too little, but 20000 is a lot
    # Creating a code block takes 7 characters
    characterLimit = 2000 - 7 if useCodeBlocks else 2000
    breakIndexes = range(0, len(fullMessage), characterLimit)
    messages = [fullMessage[i:i+characterLimit] for i in breakIndexes]
    if useCodeBlocks:
        messages = [util.markdownCodeBlock(message) for message in messages]
    for message in messages[:messageLimit]:
        await self.say(message)
    if len(messages) > messageLimit:
        await self.say(''.join((
            f'Exceeded message limit ({messageLimit}). ',
            f'{len(messages) - messageLimit} messages not sent.')))
commands.Bot.longSay = longSay

# load, unload, and reload are provided to aid in the development of the bot
# A developer can write their changes, then reload the module they changed
# without needing to restart the bot entirely by issuing `!reload <module>`

async def load(extensionName: str):
    ''' loads an extension '''
    try:
        BOT.load_extension(extensionName)
    except Exception as error:
        errorMsg = util.markdownCodeBlock(
            f'{type(error).__name__}: {error}',
            language='Python')
        await BOT.longSay(
            f'An error occurred while attempting to load:\n{errorMsg}')
        return
    await BOT.say(f'{extensionName} loaded')

async def unload(extensionName: str):
    ''' unloads an extension '''
    try:
        BOT.unload_extension(extensionName)
    except Exception as error:
        errorMsg = util.markdownCodeBlock(
            f'{type(error).__name__}: {error}',
            language='Python')
        await BOT.longSay(
            f'An error occurred while attempting to unload:\n{errorMsg}')
        return
    await BOT.say(f'{extensionName} unloaded')

async def reload(extensionName: str):
    ''' reloads an extension '''
    # convenience function
    await unload(extensionName)
    await load(extensionName)

# Adds load, unload, and reload as commands to the bot
# The decorator is not applied when these functions are declared because
# reload makes use of both load and unload, which it would not be able to do if
# they were warpped by the decorator
for command in (load, unload, reload):
    BOT.command(hidden=True)(command)

if __name__ == '__main__':
    for extension in STARTUP_EXTENSIONS:
        BOT.load_extension(extension)
    BOT.run(util.CONFIG['token'])

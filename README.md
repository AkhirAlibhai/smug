# smug
Multipurpose Discord chat/voice bot

&nbsp;

Template config.yaml file:

```yaml
---
client_id: 'smug'
token: ''
profile_picture_directory: 'profile_pictures' # relative path

audio_presets_files:
- presets.yaml
- presets.json
```

&nbsp;

Template presets file (yaml):

```yaml
---
preset 1: URL of preset 1
preset 2: URL of preset 2
preset playlist 1:
- URL 1
- URL 2
- URL 3
```

Template presets file (json):
```json
{
    "preset 1": "URL of preset 1",
    "preset 2": "URL of preset 2",
    "preset playlist 1": [
        "URL 1",
        "URL 2",
        "URL 3"
    ]
}
```

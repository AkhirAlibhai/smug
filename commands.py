''' Miscellaneous commands assembled together as a cog '''

import os
import random
import re
from datetime import datetime

from pyfiglet import Figlet
import discord
from discord.ext import commands

import util

class Commands:
    ''' Commands cog '''
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def ping(self):
        ''' Pings the bot '''
        await self.bot.say('pong!')

    @commands.command()
    async def echo(self, *, message: str):
        ''' Echoes a message '''
        await self.bot.say(message)

    @commands.command(pass_context=True)
    async def ninja(self, context, *, message: str):
        ''' Like echo, but deletes the message that issued the command '''
        await self.bot.delete_message(context.message)
        await self.bot.say(message)

    @commands.command()
    async def swapIcon(self):
        '''
        Changes the profile picture randomly. Uses the images found in the
        directory specified in the config file.
        '''
        try:
            iconsDirectory = util.CONFIG['profile_picture_directory']
        except KeyError:
            await self.bot.say('`profile_picture_directory` not set in config')
            return
        iconsPath = os.path.join(util.BASE_PATH, iconsDirectory)
        items = (os.path.join(iconsPath, x) for x in os.listdir(iconsPath))
        files = tuple(x for x in items if os.path.isfile(x))
        with open(random.choice(files), 'rb') as iconFile:
            try:
                await self.bot.edit_profile(avatar=iconFile.read())
            except Exception as error:
                # TODO: limit this except to the relevant errors
                self.bot.longSay(
                    'An error occurred while attempting to change profile'
                    f'pictures: ```py\n{type(error).__name__}: {error}\n```')
        await self.bot.say('Profile picture changed!')

    @commands.command()
    async def figlet(self, *, message: str):
        ''' Echoes a message with figlet '''
        await self.bot.say(util.markdownCodeBlock(Figlet().renderText(message)))

    @commands.command()
    async def roll(self, *, rolls: str = '1d6'):
        '''
        Rolls dice using NdN format
        Allows for multiple dice rolls using '+' as a seperator
        '''
        if re.fullmatch(util.ROLL_PATTERN, rolls) is None:
            await self.bot.say('Error: Format must be NdN [+ NdN]...')
            return
        results = []
        for dice in re.split(util.ROLL_SPLIT_PATTERN, rolls):
            numRolls, limit = map(int, dice.split('d'))
            for _ in range(numRolls):
                rollResult = random.randint(1, limit)
                results.append(rollResult)
        await self.bot.longSay(', '.join(str(x) for x in results))
        if len(results) > 1:
            await self.bot.say(f'Total is {sum(results)}')

    @commands.command(pass_context=True)
    async def userinfo(self, context, *, message: str = None):
        ''' Displays information about you, or a mentioned user '''
        if message:
            server = context.message.server
            # Given a message, tries to get a user as a mention, then tries
            # to get the user by their name
            user = (server.get_member(message.strip(' <@!>\\')) or
                    server.get_member_named(message))
            if not user:
                await self.bot.say(f'Error: Could not find user {message}')
                return
        else: # No user specified - default to the user who issued the command
            user = context.message.author
        game = f'Playing: {user.game.name}' if user.game else \
               f'Status is: {user.status.name.title()}'
        author = ''.join((str(user), ' (Bot)' if user.bot else '',
                          f' / {user.nick}' if user.nick else ''))
        embed = discord.Embed(description=game, colour=user.colour)
        embed.set_author(name=author)
        embed.set_thumbnail(url=user.avatar_url)
        embed.add_field(name='User Id', value=user.id, inline=False)
        daysSince = lambda when: (datetime.now() - when).days
        embed.add_field(
            name='Server Join Date',
            value=(f'{user.joined_at:%Y-%m-%d %H:%M:%S}\n'
                   f'({daysSince(user.joined_at)} days ago)'))
        embed.add_field(
            name='Account Creation Date',
            value=(f'{user.created_at:%Y-%m-%d %H:%M:%S}\n'
                   f'({daysSince(user.created_at)} days ago)'))
        roles = ', '.join(role.name for role in user.roles[1:])
        if roles:
            embed.add_field(name='Roles', value=roles, inline=False)
        await self.bot.say(embed=embed)

def setup(bot):
    ''' Adds the commands cog to the bot '''
    bot.add_cog(Commands(bot))

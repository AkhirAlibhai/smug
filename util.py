''' Various helper functions used by the bot '''

import re
import os

import yaml

def staticVars(**kwargs):
    ''' Decorator for assigning static variables to a function '''
    def setFuncAttr(func):
        ''' Sets the provided keyword arguments as attributes of func '''
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return setFuncAttr

def markdownCodeBlock(text: str, language: str = ''):
    ''' formats text as a markdown code block '''
    return f'```{language}\n {text}\n```'

ROLL_PATTERN = re.compile(r'([\d]+[d]{1}[\d]+)(\s*[+]\s*[\d]+[d]{1}[\d]+)*')
ROLL_SPLIT_PATTERN = re.compile(r'\s*\+\s*')

BASE_PATH = os.path.dirname(os.path.realpath(__file__))

with open(os.path.join(BASE_PATH, 'config.yaml')) as configFile:
    CONFIG = yaml.load(configFile)

AUDIO_PRESETS = dict()
try:
    audioPresetFiles = CONFIG['audio_presets_files']
    for audioPresetFileName in audioPresetFiles:
        audioPresetPath = os.path.join(BASE_PATH, audioPresetFileName)
        with open(audioPresetPath) as audioPresetFile:
            AUDIO_PRESETS.update(yaml.load(audioPresetFile))
except KeyError:
    pass
